<?php

function f($x, $m, $b)
{
    return $m * $x + $b;
}

$m = $_POST['m'];
$b = $_POST['b'];

echo "<table>";
echo "<tr><th>x</th><th>f(x)</th></tr>";
for ($x = -10; $x <= 10; $x++) {
    echo "<tr><td>$x</td><td>" . f($x, $m, $b) . "</td></tr>";
}
echo "</table>";

echo '<a href="linear-function.html">Back</a>';